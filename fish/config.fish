set fish_greeting

set TERM kitty
set EDITOR nvim

set -Ux fifc_editor nvim

alias xinstall "sudo xbps-install -Suvy"
alias xremove "sudo xbps-remove"

alias zig "/home/ly/Downloads/git/zig/zig"
alias zls "/home/ly/Downloads/git/zls/zig-out/bin/zls"

alias ls "eza -lo --icons --git --group-directories-first"

fish_add_path /home/ly/.spicetify
fish_add_path /home/ly/Downloads/git/
fish_add_path /home/ly/go/bin
fish_add_path /home/ly/.cargo/bin
fish_add_path /home/ly/Downloads/git/neovide/target/release
fish_add_path /home/ly/.local/bin
fish_add_path /home/ly/Downloads/git/browserpass-linux64-3.1.0/


