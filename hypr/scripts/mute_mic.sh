#!/usr/bin/env bash

MIC_SOURCE="$(pamixer --list-sources | grep "input" | awk '{ print $1 }')"

pamixer --source "$MIC_SOURCE" -t

MUTE_STATUS="$(pamixer --source "$MIC_SOURCE" --get-mute)"
if [[ "$MUTE_STATUS" == "true" ]]; then
    echo "" > ~/.config/hypr/scripts/muted.status
else
    echo "" > ~/.config/hypr/scripts/muted.status
fi
