ZDOTDIR=~/.zsh/

############### HISTORY

HISTSIZE=5000               #How many lines of history to keep in memory
HISTFILE=~/.zsh/.zsh_history     #Where to save history to disk
SAVEHIST=5000               #Number of history entries to save to disk
setopt    appendhistory     #Append history to the history file (no overwriting)
setopt    sharehistory      #Share history across terminals
setopt    incappendhistory  #Immediately append to the history file, not just when a term is killed

autoload -Uz compinit
compinit

############### PATH

path+=(/home/ly/go/bin/)
path+=(/home/ly/.cargo/bin/)
path+=(/home/ly/.local/bin/)
path+=(/home/ly/Downloads/git/vpsm/)
path+=(/home/ly/.dotnet/)
path+=(/home/ly/Downloads/git/pulse-browser/)
export PATH

############### VARS

export EDITOR=nvim
export RUST_MOMMY_USERS_NAMES="kitty;kitten;doll;girl;slut;princess;cumslut;whore;toy;baby;babygirl;fox;fluffball;kittyfox;bunny;bun;catbun;dolly"
export BAT_THEME="fairy_forest"
export TERM="xterm-256color"
export XBPS_DISTDIR=$HOME/.void-packages/
export DOTNET_ROOT=~/.dotnet/

############### ALIASES

alias ls='eza -1 --icons --git --group-directories-first'
alias tree='eza --tree --icons'
alias grep='rg'

alias xbps-query=xrs
alias xbps-src=vpsm
alias xsrc=vpsm

alias sudo="doas --"
alias sudoedit="~/Documents/programming/shell/doasedit"
alias doasedit="~/Documents/programming/shell/doasedit"

alias ..='cd ..'
alias ../..='cd ../..'

############### ANTIDOTE

. ~/.zsh/antidote/antidote.zsh
antidote load ${ZDOTDIR:-$HOME}/.zsh_plugins.txt

############### SHELL MOMMY

# precmd() { mommy-rs -s -e $? }

############### KEYBINDS

bindkey '\e[A' history-search-backward
bindkey '\e[B' history-search-forward

bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

############### THEME AND OPTIONS

setopt PROMPT_SUBST
setopt glob_dots

ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#3E3E43,bg=black,bold,underline"

zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}

# this is functional stuff, not just aesthetics but idk where to put them so
zstyle ':fzf-tab:*' prefix '·'
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'eza -1 --color=always $realpath'
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

export FZF_DEFAULT_OPTS=$FZF_DEFAULT_OPTS'
 --color=fg:#C9C7CD,bg:#0F1010,hl:#8EB6F5
 --color=fg+:#C9C7CD,bg+:#1B1B1B,hl+:#8EC4E5
 --color=info:#BFD7B5,prompt:#F8C8DC,pointer:#F8C8DC
 --color=marker:#BEE7c5,spinner:#C3B1E1,header:#C1E7E3'

PROMPT=$'%B%{$fg[yellow]%}%n %{$fg[green]%}at %{$fg[blue]%}%M %{$fg[green]%}in %{$fg[magenta]%}%~ %{$fg[red]%}%{$reset_color%} $(gitprompt)%f
%{$fg[red]%}ζ%b '

# opam configuration
[[ ! -r /home/ly/.opam/opam-init/init.zsh ]] || source /home/ly/.opam/opam-init/init.zsh  > /dev/null 2> /dev/null

# ghcup-env
[ -f "/home/ly/.ghcup/env" ] && source "/home/ly/.ghcup/env"
