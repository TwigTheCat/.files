import asyncio
from libqtile import hook
import os

from core.bar import bar

margin = sum(bar.margin) if bar else -1


@hook.subscribe.startup
def startup():
    if margin == 0:
        bar.window.window.set_property(
            name="WM_NAME",
            value="QTILE_BAR",
            type="STRING",
            format=8,
        )
    os.system('xsetwacom set "HUION Huion Tablet Pad pad" Button 1 "key + space"')
    os.system('xsetwacom set "HUION Huion Tablet Pad pad" Button 2 "key +space"')
    os.system('xsetwacom set "HUION Huion Tablet Pad pad" Button 3 "key ctrl shift +z"')
    os.system('xsetwacom set "HUION Huion Tablet Pad pad" Button 8 "key ctrl +z" ')
    # os.system('bash ~/Programming/Go/meww_go/run')
    os.system("mpris-proxy &")
    os.system("kill picom")
    os.system("picom &")
    os.system("dunst &")


@hook.subscribe.client_new
async def client_new(client):
    await asyncio.sleep(0.5)
    if client.name == "Spotify":
        client.togroup("q")
    elif client.name == "ArmCord":
        client.togroup("4")
    elif client.name == "waterfox-g":
        client.togroup("3")
    elif client.name == "steam":
        client.togroup("w")


@hook.subscribe.client_new
async def client_new_opacity(client):
    if client.name == "ArmCord" or client.get_wm_class() == ["firefox"]:
        client.cmd_opacity(1)
    else:
        client.cmd_opacity(0.9)
