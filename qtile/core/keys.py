from libqtile.config import Key
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

from extras import float_to_front
from utils import config

keys, mod, alt = [], 'mod4', 'mod1'
terminal = config['terminal'].copy()

if not terminal['main']:
    terminal['main'] = guess_terminal()

for key in [
    # Switch between windows
    ([mod], 'Down', lazy.layout.down()),
    ([mod], 'Up', lazy.layout.up()),

    # Move windows between columns
    ([mod, 'shift'], 'Left', lazy.screen.prev_group()),
    ([mod, 'shift'], 'Right', lazy.screen.next_group()),

    # Increase/decrease window size
    ([mod], 'Left', lazy.layout.shrink()),
    ([mod], 'Right', lazy.layout.grow()),

    # Window management
    ([mod, 'shift'], 'space', lazy.layout.flip()),
    ([mod], 'o', lazy.layout.maximize()),
    ([mod], 'n', lazy.layout.normalize()),
    (['control', 'shift'], 'q', lazy.window.kill()),
    ([mod], 'f', lazy.window.toggle_fullscreen()),

    # Floating window management
    ([mod], 'space', lazy.window.toggle_floating()),
    ([mod], 'c', lazy.window.center()),

    # Toggle between layouts
    ([mod], 'Tab', lazy.next_layout()),

    # Qtile management
    ([mod, 'control'], 'b', lazy.hide_show_bar()),
    ([mod, 'control'], 'q', lazy.shutdown()),
    ([mod, 'shift'], 'r', lazy.reload_config()),
    ([mod, 'control'], 'r', lazy.restart()),

    # Kill X11 session
    ([mod, alt], 's', lazy.spawn('kill -9 -1')),

    # Terminal
    ([mod, 'shift'], 'Return', lazy.spawn(terminal['main'])),

    # Application Launcher
    ([mod], 'r', lazy.spawn('rofi -show drun')),

    # Screenshot Tool
    (['control', 'shift'], 'f', lazy.spawn('flameshot gui')),

    # Backlight
    ([mod], 'XF86AudioLowerVolume', lazy.spawn('brightnessctl set 5%-')),
    ([mod], 'XF86AudioRaiseVolume', lazy.spawn('brightnessctl set +5%')),

    # Volume
    ([], 'XF86AudioMute', lazy.spawn('pamixer --toggle-mute')),
    ([], 'XF86AudioLowerVolume', lazy.spawn('pamixer --decrease 5')),
    ([], 'XF86AudioRaiseVolume', lazy.spawn('pamixer --increase 5')),

    # Player
    ([], 'XF86AudioPlay', lazy.spawn('playerctl play-pause')),
    ([], 'XF86AudioPrev', lazy.spawn('playerctl previous')),
    ([], 'XF86AudioNext', lazy.spawn('playerctl next')),
]:
    keys.append(Key(*key))  # type: ignore
