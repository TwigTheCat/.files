from libqtile.bar import CALCULATED
from libqtile.lazy import lazy
from libqtile.widget import *

from core.bar.utils import base, decoration, iconFont, powerline
from extras import Clock, GroupBox, modify, TextBox, Volume, widget
from utils import color

tags: list[str] = [
    "󰅩",
    "",
    "",
    "󰙯",
    "",
    "",
    "",
]

bar: dict = {
    "background":   "#1e1e2e00",
    "foreground":   "#1e1e2e00",
    "border_width": 0,
    "margin": [1, 0, 0, 0],
    "size": 26,
}


def sep(fg: str, offset=0, padding=8) -> TextBox:
    return TextBox(
        **base(None, fg),
        **iconFont(),
        offset=offset,
        padding=padding,
        text="",
    )


def logo(bg: str, fg: str) -> TextBox:
    return modify(
        TextBox,
        **base(bg, fg),
        **decoration(),
        **iconFont(),
        mouse_callbacks={"Button1": lazy.restart()},
        offset=4,
        padding=17,
        text="",
    )


def groups(bg: str) -> GroupBox:
    return GroupBox(
        **iconFont(),
        background=bg,
        borderwidth=1,
        colors=[
            color["cyan"],
            color["magenta"],
            color["yellow"],
            color["blue"],
            color["green"],
            color["red"],
            color["white"],
        ],
        highlight_color=color["bg"],
        highlight_method="line",
        inactive=color["fg"],
        invert=True,
        padding=7,
        rainbow=True,
    )


def volume(bg: str, fg: str) -> list:
    return [
        modify(
            TextBox,
            **base(bg, fg),
            **decoration("left"),
            **iconFont(),
            text="",
            x=4,
        ),
        modify(
            Volume,
            **base(bg, fg),
            **powerline("arrow_right"),
            commands={
                "decrease": "pamixer --decrease 5",
                "increase": "pamixer --increase 5",
                "get": "pamixer --get-volume-human",
                "mute": "pamixer --toggle-mute",
            },
            update_interval=0.1,
        ),
    ]


def updates(bg: str, fg: str) -> list:
    return [
        TextBox(
            **base(bg, fg),
            **iconFont(),
            offset=-1,
            text="",
            x=-5,
        ),
        widget.CheckUpdates(
            **base(bg, fg),
            **decoration("right"),
            colour_have_updates=fg,
            colour_no_updates=fg,
            display_format="{updates} updates  ",
            distro="Arch_checkupdates",
            initial_text="No updates  ",
            no_update_string="No updates  ",
            padding=0,
            update_interval=3600,
        ),
    ]


def window_name(bg: str, fg: str) -> object:
    return widget.WindowName(
        **base(bg, fg),
        format="{name}",
        max_chars=30,
        width=CALCULATED,
    )


def cpu(bg: str, fg: str) -> list:
    return [
        modify(
            TextBox,
            **base(bg, fg),
            **iconFont(),
            offset=3,
            text="",
            x=5,
        ),
        widget.CPU(
            **base(bg, fg),
            **powerline("arrow_right"),
            format="{load_percent:.0f}%",
        ),
    ]


def ram(bg: str, fg: str) -> list:
    return [
        TextBox(
            **base(bg, fg),
            **iconFont(),
            offset=-2,
            padding=5,
            text="",
            x=-2,
        ),
        widget.Memory(
            **base(bg, fg),
            **powerline("arrow_right"),
            format="{MemUsed: .0f}{mm} ",
            padding=-1,
        ),
    ]


def disk(bg: str, fg: str) -> list:
    return [
        TextBox(**base(bg, fg), **iconFont(), offset=2, text="", padding=5),
        widget.DF(
            **base(bg, fg),
            **decoration("right"),
            format="{f} GB  ",
            padding=5,
            partition="/",
            visible_on_warn=False,
            warn_color=fg,
        ),
    ]


def clock(bg: str, fg: str) -> list:
    return [
        modify(
            Clock,
            **base(bg, fg),
            **decoration("center"),
            format="%A - %I:%M %p ",
            long_format="%B %-d, %Y ",
            padding=6,
        ),
    ]


def mpris(bg: str, fg: str) -> list:
    return [
        widget.Mpris2(
            width=450,
            **base(bg, fg),
            **decoration("center"),
            stop_pause_text="Paused",
            scroll_chars=75,
            scroll_interval=0.1,
            scroll_wait_interval=1,
            display_metadata=["xesam:title", "xesam:artist"],
        ),
    ]


def mpd(bg: str, fg: str) -> list:
    return [
        widget.Mpd2(
                status_format='{play_status} {artist} -{title} [{album}]',
                **base(bg, fg),
                interval=1.0
        )
    ]


widgets: list = [
    widget.Spacer(length=6),
    logo(color["blue"], color["bg"]),
    sep(color["black"], offset=-8),
    groups(None),
    sep(color["black"]),
    *mpris(color["magenta"], color["bg"]),
    widget.Spacer(),
    *clock(color["magenta"], color["bg"]),
    widget.Spacer(),
    *volume(color["magenta"], color["bg"]),
    *cpu(color["green"], color["bg"]),
    *ram(color["yellow"], color["bg"]),
    *disk(color["cyan"], color["bg"]),

    widget.Spacer(length=6),
]
