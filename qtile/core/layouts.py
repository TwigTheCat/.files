from libqtile import layout
from libqtile.config import Match

from utils import color

# ---- Tiling ---------------------------- #
config = {
    "border_focus": color["magenta"],
    "border_normal": color["white"],
    "border_width": 2,
    "margin": 5,
    "single_border_width": 2,
    "single_margin": 5,
}

layouts = [
    layout.MonadTall(
        **config,
        change_ratio=0.02,
        min_ratio=0.30,
        max_ratio=0.70,
    ),
    layout.Max(**config),
]

# ---- Floating -------------------------- #
floating_layout = layout.Floating(
    border_focus=color["white"],
    border_normal=color["bg"],
    border_width=2,
    fullscreen_border_width=0,
    float_rules=[],
)
